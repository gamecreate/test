﻿#pragma strict

//初期の画面サイズを設定　Phone5sで設定
var fixWidth  : float = 640.0;		//横幅
var fixHeight  : float = 1136.0;		//縦幅
//縦横補正用 倍率で指定 1.2倍等
var corTall : float = 1.0;
var corWide : float = 1.0;

var imgW : float;
var imgH : float;
var imgX : float;
var imgY : float;

function Start () {
	//初期の画像サイズ取得
	imgX = guiTexture.pixelInset.x;
	imgY = guiTexture.pixelInset.y;
	imgW = guiTexture.pixelInset.width;
	imgH = guiTexture.pixelInset.height;
}

function Update () {
	//フォントサイズ変更用関数呼び出し
	imgResize ();
}

function imgResize (){
		//現在のscreen Size取得
	var scWidth : float = Screen.width;
	var scHeight : float= Screen.height;
	var winAspect : float = scWidth / scHeight;
	
	//初期の画面サイズと現在の画面サイズの比率を計算 	
	var wdRatio : float = 100.0 / (fixWidth / scWidth);
	var heRatio : float = 100.0 / (fixHeight / scHeight);
	
	//縦横時の判別
	var ratio : float;//倍率
	if(scWidth < scHeight){
		//tallの場合は横で比率を合わせる
		ratio = wdRatio * corTall;
	}else{
		//wideの場合は縦で比率を合わせる
		ratio = heRatio * corWide;
	}
	
	//リサイズ表示サイズ
	var reImgSizeW : int = imgW * (ratio  / 100);
	var reImgSizeH : int = imgH * (ratio / 100);
	//リサイズ画像位置
	var reImgSizeX : int = imgX * (ratio / 100);
	var reImgSizeY : int = imgY * (ratio / 100);
	
	//1つずつ値を入れる場合
	//guiTexture.pixelInset.x = reImgSizeX;
	//guiTexture.pixelInset.y = reImgSizeY;
	//guiTexture.pixelInset.width = reImgSizeW;
	//guiTexture.pixelInset.height = reImgSizeH;
	
	//まとめて値を入れる
	guiTexture.pixelInset = Rect(reImgSizeX,reImgSizeY,reImgSizeW,reImgSizeH);

}