﻿var skin :GUISkin;

@HideInInspector

var score:int;

function OnGUI(){
	GUI.skin = skin;
	var sw = Screen.width;
	var sh = Screen.height;
	var scoreText: String = "SCORE: "+score.ToString();
	GUI.Label(Rect(0 , 0 , sw / 2, sh / 4),scoreText,"score");
}